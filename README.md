# MOI - The "More Optimized Image Format" for fast, lossless image compression

This repo contains the specification and an implementation of MOI, an image format designed to beat PNG in performance, size, and simplicity.  

MOI was inspired by QOI, it takes the byte aligned tag encoding style of QOI and adds and tweaks some opcodes to achieve better compression while sacrificing some performance.  

```
fmt  | encode  | decode  | size
moi  | 20.745s | 11.192s | 21.65%
qoi  |  6.774s |  5.712s | 25.58%
png5 | 111.28s | 15.128s | 23.66%
png9 | 512.90s | 15.021s | 22.92%
```

Benchmark timings were collected on an AMD Ryzen 7 3750H  
The same [test suite](https://qoiformat.org/benchmark/qoi_benchmark_suite.tar) as QOI was used, ignoring two images with broken checksums.  
Benchmark repository [here](https://gitlab.com/terepy/moi_bench)  

See [the specification](https://gitlab.com/terepy/moi/-/blob/master/specification.md) for details on algorithm and opcodes.
