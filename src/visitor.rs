use crate::Image;
use std::mem::{MaybeUninit,ManuallyDrop};

const CHUNK_COORD: [(usize, usize); 64] = [
    (0, 0), (0, 1), (1, 1), (1, 0), //top left
    (2, 0), (3, 0), (3, 1), (2, 1),
    (2, 2), (3, 2), (3, 3), (2, 3),
    (1, 3), (1, 2), (0, 2), (0, 3),
    
    (0, 4), (1, 4), (1, 5), (0, 5), //bottom left
    (0, 6), (0, 7), (1, 7), (1, 6),
    (2, 6), (2, 7), (3, 7), (3, 6),
    (3, 5), (2, 5), (2, 4), (3, 4),
    
    (4, 4), (5, 4), (5, 5), (4, 5), //bottom right
    (4, 6), (4, 7), (5, 7), (5, 6),
    (6, 6), (6, 7), (7, 7), (7, 6),
    (7, 5), (6, 5), (6, 4), (7, 4),
    
    (7, 3), (7, 2), (6, 2), (6, 3), //top right
    (5, 3), (4, 3), (4, 2), (5, 2),
    (5, 1), (4, 1), (4, 0), (5, 0),
    (6, 0), (6, 1), (7, 1), (7, 0),
];

struct ChunkVisitor {
    width: usize,
    height: usize,
    i: usize,
    x: usize,
    y: usize,
    next: usize,
}

impl ChunkVisitor {
    fn new(width: u32, height: u32) -> Self {
        Self {
            width: width as usize,
            height: height as usize,
            i: 0,
            x: 0,
            y: 0,
            next: 0,
        }
    }
    
    #[inline]
    fn next(&mut self) -> Option<usize> {
        let r = self.peek();
        loop {
            self.i += 1;
            if self.i == CHUNK_COORD.len() {
                self.i = 0;
                self.x += 8;
                if self.x  >= self.width {
                    self.x = 0;
                    self.y += 8;
                    if self.y >= self.height {
                        self.next = usize::max_value();
                        break;
                    }
                }
            }
            let x = self.x + CHUNK_COORD[self.i].0;
            let y = self.y + CHUNK_COORD[self.i].1;
            if x < self.width && y < self.height {
                self.next = x + self.width * y;
                break;
            }
        }
        r
    }

    #[inline]
    fn peek(&self) -> Option<usize> {
        if self.next < usize::max_value() {
            Some(self.next)
        } else {
            None
        }
    }
}

pub struct PixelVisitor<'a> {
    visitor: ChunkVisitor,
    pixels: &'a [[u8; 4]],
}

impl<'a> PixelVisitor<'a> {
    pub fn new(width: u32, height: u32, pixels: &'a [[u8; 4]]) -> Self {
        Self {
            visitor: ChunkVisitor::new(width, height),
            pixels,
        }
    }
    
    #[inline(always)]
    pub fn next(&mut self) -> Option<[u8; 4]> {
        self.visitor.next().map(|i| self.pixels[i])
    }

    #[inline(always)]
    pub fn peek(&mut self) -> Option<[u8; 4]> {
        self.visitor.peek().map(|i| self.pixels[i])
    }
}

pub struct PixelWriter {
    visitor: ChunkVisitor,
    pixels: Vec<MaybeUninit<[u8; 4]>>,
}

impl PixelWriter {
    pub fn new(width: u32, height: u32) -> Self {
        let len = width as usize * height as usize;
        let mut pixels = Vec::with_capacity(len);
        unsafe {
            pixels.set_len(len);
        }
        
        Self {
            visitor: ChunkVisitor::new(width, height),
            pixels,
        }
    }
    
    #[inline]
    pub fn output(&mut self, pixel: [u8; 4]) {
        if let Some(i) = self.visitor.next() {
            unsafe {
                *self.pixels.get_unchecked_mut(i) = MaybeUninit::new(pixel);
            }
        }
    }
    
    #[inline(always)]
    pub fn finished(&self) -> bool {
        self.visitor.peek().is_none()
    }
    
    pub fn finish(self) -> Result<Image, &'static str> {
        if self.finished() { //we must check this or else we are returning unset memory
            let mut pixels = ManuallyDrop::new(self.pixels);
            Ok(Image {
                width: self.visitor.width as u32,
                height: self.visitor.height as u32,
                pixels: unsafe { Vec::from_raw_parts(pixels.as_mut_ptr() as *mut _, pixels.len(), pixels.capacity()) },
            })
        } else {
            Err("missing data")
        }
    }
}
