//tweakable
pub const INDEX_MAX: usize = 64;
pub const HISTORY_SIZE: usize = 256;
pub const DOUBLE_MAX: u8 = 16;
pub const TRIPLE_MAX: u8 = 8;
pub const QUAD_MAX: u8 = 4;
pub const RUN_MAX: usize = 9;
pub const LUMA_RANGE: u8 = 5;
pub const LUMA2_RANGE: u8 = 48;
pub const DIFFU_RANGE: u8 = 4;
pub const RGB_MAX: usize = 4;

pub const LUMA_BIAS: u8 = LUMA_RANGE / 2;
pub const LUMA2_BIAS: u8 = LUMA2_RANGE / 2 - 1;

//not tweakable
pub const DIFFU_MIN: u8 = 3;
pub const LONG_RUN_MIN: usize = RUN_MAX * 2 - 1;
pub const LONG_RUN_MAX: usize = LONG_RUN_MIN + INDEX_MAX - 2;

//opcodes
pub const INDEX: u8 = 0; //00xx_xxxx
pub const INDEX_END: u8 = INDEX + INDEX_MAX as u8;
pub const DOUBLE: u8 = INDEX_END; //0100_xxxx
pub const DOUBLE_END: u8 = DOUBLE + DOUBLE_MAX;
pub const TRIPLE: u8 = DOUBLE_END; //0101_0xxx
pub const TRIPLE_END: u8 = TRIPLE + TRIPLE_MAX;
pub const QUAD: u8 = TRIPLE_END; //0101_10xx
pub const QUAD_END: u8 = QUAD + QUAD_MAX;
pub const RUN: u8 = QUAD_END; //0101_11xx
pub const RUN_END: u8 = RUN + RUN_MAX as u8 - 5;
pub const LUMA3: u8 = RUN_END; //0110_xxxx
pub const LUMA3_END: u8 = LUMA3 + 16;
pub const DIFFU: u8 = LUMA3_END; //0111_0xxx
pub const DIFFU_END: u8 = DIFFU + DIFFU_RANGE * 2;
pub const RGB: u8 = DIFFU_END; //0111_10xx
pub const RGB_END: u8 = RGB + RGB_MAX as u8;
pub const RGBA: u8 = RGB_END; //0111_1100
pub const RGBA2: u8 = RGBA + 1; //0111_1101
pub const UTIL: u8 = RGBA2 + 1; //0111_1110
pub const DIFF200: u8 = UTIL + 1; //0111_1111
pub const LUMA: u8 = DIFF200 + 1; //10xx_xxxx and 1100_xxxx
pub const LUMA_END: u8 = LUMA + LUMA_RANGE * 16;
pub const LUMA2: u8 = LUMA_END; //1101_xxxx and 111x_xxxx
