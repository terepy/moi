# the more optimized image format  

a MOI file uses a 12 byte header followed by any number of bytes encoded by the moi algorithm  

the header contains:  
-the magic bytes `moi`  
-a one byte version number (currently 0)  
-two 4 byte little endian numbers for the width and height  

the image is encoded in 8 by 8 chunks, read/written left to right, top to bottom  
each pixel in the chunk is encoded in this (hilbert curve) order:  
```
(0, 0), (0, 1), (1, 1), (1, 0), //top left
(2, 0), (3, 0), (3, 1), (2, 1),
(2, 2), (3, 2), (3, 3), (2, 3),
(1, 3), (1, 2), (0, 2), (0, 3),

(0, 4), (1, 4), (1, 5), (0, 5), //bottom left
(0, 6), (0, 7), (1, 7), (1, 6),
(2, 6), (2, 7), (3, 7), (3, 6),
(3, 5), (2, 5), (2, 4), (3, 4),

(4, 4), (5, 4), (5, 5), (4, 5), //bottom right
(4, 6), (4, 7), (5, 7), (5, 6),
(6, 6), (6, 7), (7, 7), (7, 6),
(7, 5), (6, 5), (6, 4), (7, 4),

(7, 3), (7, 2), (6, 2), (6, 3), //top right
(5, 3), (4, 3), (4, 2), (5, 2),
(5, 1), (4, 1), (4, 0), (5, 0),
(6, 0), (6, 1), (7, 1), (7, 0),
```
any pixels that lie outside the image are skipped  

# algorithm  

the encoder takes a stream of pixels and outputs a stream of bytes  
the decoder takes a stream of bytes and outputs a stream of pixels  

the encoder and decoder keep track of a history buffer of 256 pixels, initialized to zero  
when a pixel is added to the history, the nth pixel goes to the n+1th slot, and the new pixel is placed in slot 0, eg:  
`history.rotate_right(1); history[0] = pixel` or `for (int i = 255; i > 0; i--) { history[i] = history[i-1]; } history[0] = pixel`  
when a pixel is moved to the front of the history, the nth pixel goes to the n+1th slot, and the pixel is placed in slot 0 eg:  
`history[0..=i].rotate_right(1)` or `temp = history[i]; while i > 0 { history[i] = history[i-1]; i--; } history[0] = temp`  
all integer operations are unsigned 8-bit wrapping  

# opcodes  
these are the ways pixels can be encoded, everything is byte-aligned:  

index: `00nn_nnnn`  
the nth pixel in the history should be moved to the front and output  

double: `0100_nnnn`  
same as index, except the nth pixel should be output twice  

triple: `0101_0nnn`  
same as index, except the nth pixel should be output three times  

quad: `0101_10nn`  
same as index, except the nth pixel should be output four times  

run: `0101_11nn`  
the first pixel in the history should be output n+5 times  

luma3: `0110_rrrr, rrbb_bbbb, gggg_gggg`  
output should be a difference from the first pixel in the history  
the `g` bits specify the difference in green channel  
if the green difference is greater than 127, then the bias for the other channels is 31, otherwise it is 32  
the `r` and `b` bits specify the difference in red and blue channels minus the difference in the green channel, stored with the bias  
so the red difference is given by `dr = r + g - bias` and blue by `dg = b + g - bias`  
the output is the first pixel plus each channel's difference  
the output should be added to the history  

diffu: `0111_0sxx`  
output should be a difference from the first pixel in the history  
if the `s` bit is 1, then the change is `3 + x`  
if the `s` bit is 0, then the change is `253 - x`  
the output is the first pixel in the history with the change added to the `rgb` channels  
the output should be added to the history  

rgb: `0111_10nn, red, green, blue, red, ...`  
the next n+1 pixels have their `rgb` values encoded directly in the following bytes  
the alpha value is the same as the first pixel in the history  
each pixel should be output and added to the history, in the order they are laid out  

rgba: `0111_1100, red, green, blue, alpha`  
the next pixel is encoded directly in the next 4 bytes  
the pixel should be output and added to the history  

rgba2: `0111_1101, red, green, blue, alpha, red, green, blue, alpha`  
the next two pixels are encoded directly in the next 8 bytes  
the pixels should be output and added to the history in the order they are laid out  

util: `0111_1110, nnnn_nnnn, ...`  
if n is less than 62, then the first pixel in history should be output n+17 times  
if n is 62, then the next byte should be read and the first pixel should be output 79+byte times  
if n is 63 then the next two bytes should be read and interpreted as a 16 bit (little endian) number and the first pixel should be output this many times plus 335  
if n is 64 or more, then the nth pixel in the history should be moved to the front and output  

diff200: `0111_1111`  
output is the first pixel in the history, with 2 added to its red channel  
the output should be added to the history  

luma1: `1ggg_rrbb` where `ggg` is less than `101`  
same as luma3, output should be a difference from the first pixel in the history  
the green difference is stored with a bias of 2: `dg = g - 2`  
if the green difference is greater than 127, then the bias for the other channels is 1, otherwise it is 2  
if all the changes are zero, the blue change should be set to 2  
note that the `g` bits cannot be `101`,`110` or `111` as these conflict with luma2  

luma2: `11gg_gggg, rrrr_bbbb` where `gg_gggg` is greater than `00_1111`  
same as luma3, output should be a difference from the first pixel in the history  
the green difference is stored with a bias of 39: `dg = g - 39`  
if the green difference is greater than 127, then the bias for the other channels is 7, otherwise it is 8  
note that the first two `g` bits cannot both be zero as this conflicts with luma1  
